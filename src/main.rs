pub mod types;
mod parser;
mod player;
use types::CellType::*;

use std::io;
use types::Move;



fn issue_order(m: Move) {
    let s = format!("place_move {} {}", m.col, m.row);
    print!("{}",s);
    println!("");

}


fn main() {
    let s = io::stdin();
    let br = s.lock();
    let mut p = parser::Parser::new(br);

    let mut game = types::Game::default();
    let mut game =
        types::Game {
            board: types::Board{data: vec![Empty; 1]},
            round: 0,
            turn: 0,
            my_points: 0,
            opponent_points: 0,
            last_update: 0,
            last_timebank: 10000,
            legal_moves: vec![false; 1],
        }
    ;
    let mut settings = types::Settings::default();

    loop {
        match p.parse_until_action(&mut settings, &mut game) {
            Ok(a) => match a {
                parser::Action::Move => issue_order(player::make_move(&mut settings, &mut game)),
                parser::Action::Quit => break,
            },
            Err(e) => {
                println!("Parsing error: {}", e.message);
                break;
            },
        }
    }
}
