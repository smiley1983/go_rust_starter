extern crate rand;

use self::rand::Rng;

use types;
use types::Move;
use types::CellType::*;

use ::std::io::Write;

const NORTH:(i8,i8) = (-1, 0);
const EAST:(i8,i8) = (0, 1);
const SOUTH:(i8,i8) = (1, 0);
const WEST:(i8,i8) = (0, -1);

macro_rules! println_stderr(
    ($($arg:tt)*) => (
        match writeln!(&mut ::std::io::stderr(), $($arg)* ) {
            Ok(_) => {},
            Err(x) => panic!("Unable to write to stderr: {}", x),
        }
    )
);

/*
I was writing the code to prevent suicide moves when I gave up on Rust for now.

pub fn neighbour (settings: &mut types::Settings, (off_row, off_col):(i8, i8), (row, col):(usize, usize)) -> Option<(usize, usize)>{
    let nr = row as i8 + off_row;
    let nc = col as i8 + off_col;
    if (nr < 0) || (nr > (settings.field_width - 1) as i8) || (nc < 0) || (nc > (settings.field_height - 1) as i8) {
        return None;
    }
    else {
        return Some ((nr as usize, nc as usize))
    }
}

pub fn neighbours (settings: &mut types::Settings, row:usize, col:usize) -> (Option<(usize, usize)>, Option<(usize, usize)>, Option<(usize, usize)>, Option<(usize, usize)>) {
    return 
        (neighbour (settings, NORTH, (row, col)),
        neighbour (settings, EAST, (row, col)),
        neighbour (settings, SOUTH, (row, col)),
        neighbour (settings, WEST, (row, col)))
//    return [north_nbr (row, col), east_nbr (row, col), south_nbr (row,col), west_nbr (row,col)]
}

pub fn set_grid_value<T> (settings: &mut types::Settings, grid: &mut Vec<T>, val:T, row:usize, col:usize) {
    let index = row * settings.field_width as usize + col;
    grid[index] = val;
}

pub fn get_grid_value<'a, T> (settings: &mut types::Settings, grid: &'a mut Vec<T>, row:usize, col:usize) -> &'a T {
    let index = row * settings.field_width as usize + col;
    return &grid[index];
}

pub fn get_cell (settings: &mut types::Settings, board: &mut types::Board, row:usize, col:usize) -> types::CellType { 
    let index = row * settings.field_width as usize + col;
    return board.data[index];
}

// It's at this point that I begin to despair and consider moving to a
// different language. SBCL is up next.
pub fn dfs (settings: &mut types::Settings, board: &mut types::Board, row:usize, col:usize) -> Vec<bool> {
    let mut result = vec!(false; board.data.len());
    let mut visited = vec!(false; board.data.len());
    let fill_colour = get_cell(settings, board, row, col);
    fn step(settings: &mut types::Settings, board: &mut types::Board, visited: &mut Vec<bool>, result: &mut Vec<bool>, fill_colour: types::CellType, loc:Option<(usize, usize)>) -> () {
//    let step = |loc| -> () {
        match loc {
            None => (),
            Some ((r, c)) => {
                if (!(get_grid_value(settings, &visited, r, c))) {
                    set_grid_value(settings, &visited, true, r, c);
                    if (*get_grid_value(settings, &board.data, r, c) == fill_colour) {
                        set_grid_value(settings, &result, true, r, c);
                        let (nN, nE, nS, nW) = neighbours(settings, r, c);
                        step(settings, board, visited, result, fill_colour, nN);
                        step(settings, board, visited, result, fill_colour, nE);
                        step(settings, board, visited, result, fill_colour, nS);
                        step(settings, board, visited, result, fill_colour, nW);
                    }
                }
            }
        }
    };
    step (settings, board, visited, result, fill_colour, Some ((row, col)));
    return result;
}

pub fn not_suicide (settings: &mut types::Settings, game: &mut types::Game, row:usize, col:usize) -> bool {
    let mut board_copy = (game.board).clone();
    return true;
}
*/

pub fn is_empty (settings: &mut types::Settings, game: &mut types::Game, row:usize, col:usize) -> bool {
    let board_index = row * settings.field_width as usize + col;

    return game.board.data[board_index] == Empty;
}


pub fn is_legal (settings: &mut types::Settings, game: &mut types::Game, row:usize, col:usize) -> bool {
    return is_empty (settings, game, row, col);// && not_suicide (settings, game, row, col);
}

pub fn update_legal_moves (settings: &mut types::Settings, game: &mut types::Game) -> () {
    for row in 0..settings.field_height {
        for col in 0..settings.field_width {
            let index = row * settings.field_width + col;
            game.legal_moves[index as usize] = is_legal (settings, game, row as usize, col as usize);
        }
    }
}


pub fn debug_legal_moves (settings: &mut types::Settings, game: &types::Game) -> () {
    let mut result:String = "".to_string();
    let mut count:usize = 0;
    for i in 0..game.legal_moves.len() {
        if count >= settings.field_width as usize {
            result = result + "\n";
            count = 0;
        };
        count = count + 1;
        match game.legal_moves[i] {
            true => result = result + "# ",
            false => result = result + ". "
        }
    }
    println_stderr!("{}", result);
}


pub fn num_legal_moves (game: &mut types::Game) -> usize {
    return (&game.legal_moves).into_iter().fold(0, |acc, v| if *v {acc + 1} else {acc})
}

pub fn select_move(settings: &mut types::Settings, game: &mut types::Game, mut rng: rand::ThreadRng) -> types::Move {
    let num_legal:usize = num_legal_moves(game);
    println_stderr!("num_legal {}", num_legal );
    debug_legal_moves(settings, game);
    let selected:usize = (rng.gen::<usize>()) % num_legal;
//    println_stderr!("selected {}", selected );
    let (index, _) = (&game.legal_moves).into_iter().fold((0, 0), 
        |(acc_all, acc_legal), v| 
        if acc_legal > selected {(acc_all, acc_legal)} 
        else if acc_legal == selected {
            if *v {
                (acc_all, acc_legal + 1)
            } else {
                (acc_all + 1, acc_legal)
            }
        }
        else {(acc_all + 1, (acc_legal + (if *v {1} else {0})))}
    );
    let row = ((index ) / settings.field_width) as i8;
    let col = ((index ) % settings.field_width) as i8;
    let player_move = types::Move{row : row, col : col};
//    println_stderr!("place_move {} {}\n", col, row);
    return player_move;
}

pub fn make_move(settings: &mut types::Settings, game: &mut types::Game) -> types::Move {
    let mut rng = rand::thread_rng();
    update_legal_moves(settings, game);
    let player_move = select_move(settings, game, rng);
//    let player_move = types::Move::default();
    return player_move;

}
